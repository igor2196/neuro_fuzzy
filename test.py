import numpy as np

from neuro_fuzzy import FuzzyNetwork
from neuro_fuzzy.structures import Antecedent, Consequent, Rule

universe = np.arange(0, 11, 1)

quality = Antecedent("Качество еды", universe)
service = Antecedent("Качество обслуживания", universe)
tip = Consequent("Чаевые", universe)

lo = "Плохо"
md = "Средне"
hi = "Хорошо"

quality[lo] = [0, 0, 5]
quality[md] = [0, 5, 10]
quality[hi] = [5, 10, 10]

service[lo] = [0, 0, 5]
service[md] = [0, 5, 10]
service[hi] = [5, 10, 10]

tip[lo] = [0, 0, 5]
tip[md] = [0, 5, 10]
tip[hi] = [5, 10, 10]

rules = [
    Rule(quality[lo] & service[lo], tip[lo]),
    Rule(quality[lo] & service[md], tip[lo]),
    Rule(quality[md] & service[lo], tip[lo]),
    Rule(quality[md] & service[md], tip[md]),
    Rule(service[md] & service[hi], tip[md]),
    Rule(quality[hi] & service[lo], tip[md]),
    Rule(quality[md] & service[hi], tip[md]),
    Rule(service[hi] & service[hi], tip[hi]),
    Rule(service[hi] & service[md], tip[hi]),
]

values = [quality.connect(6.5), service.connect(9.8)]

net = FuzzyNetwork(rules)
net.activate(values)
