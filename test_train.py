import pandas as pd

from neuro_fuzzy import FuzzyTrainer
from neuro_fuzzy.structures import FuzzyDataset

from test import net, quality, service

ds = FuzzyDataset([quality, service])
df = pd.read_csv("dataset_fuz.csv")
ds.addSamplesFromPandas(df)

trainer = FuzzyTrainer(net, ds)
trainer.train()
