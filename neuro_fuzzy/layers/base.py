from abc import ABC, abstractmethod


class Layer(ABC):
    """Базовый слой"""

    @abstractmethod
    def activate(self, **kwarg):
        raise NotImplementedError

    @abstractmethod
    def _forwardImplementation(self, **kwarg):
        raise NotImplementedError


class TraningLayer(Layer, ABC):
    """Базовый обучаемый слой"""

    @abstractmethod
    def train(self, **kwarg):
        raise NotImplementedError

    @abstractmethod
    def _backwardImplementation(self, **kwarg):
        raise NotImplementedError
