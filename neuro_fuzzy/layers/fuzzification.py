import skfuzzy as fuzz
from numpy import ndarray
from abc import ABC

from .base import TraningLayer

from ..structures import Term
from ..errors import ERRORS


class FuzzificationLayer(TraningLayer, ABC):
    """Базовый слой фазификации"""

    result = None
    membership_function = None

    @classmethod
    def calcMembf(cls, term: Term):
        return cls(term)._forwardImplementation(term.universe, term.parameters)

    def __init__(self, term: Term):
        self.term = term

    def activate(self, value: float):
        self.membership_function = self._forwardImplementation(
            self.term.universe, self.term.parameters,
        )
        self.result = fuzz.interp_membership(
            self.term.universe, self.membership_function, value
        )
        return self.result

    def train(self, value, error, speed):
        d_params = self._backwardImplementation(value, self.term.parameters)
        # TODO: Проблема с обучением

        for i, el in enumerate(zip(self.term.parameters, d_params)):
            new_val = el[0] - error * el[1] * speed

            if i == 0 and new_val < 0:
                continue
            elif new_val < self.term.parameters[i - 1]:
                continue

            self.term.parameters[i] = new_val

    @property
    def lang(self):
        return self.term.lang


class TrimFuzzificationLayer(FuzzificationLayer):
    """Треугольный слой фазификации"""

    def _forwardImplementation(self, universe: ndarray, parameters: ndarray):
        assert len(parameters) == 3, ERRORS["count_arg"]
        return fuzz.trimf(universe, parameters,)

    def _backwardImplementation(self, value, parameters: ndarray):
        a, b, c = parameters
        if a < value < b:
            dy_da = (value - b) / ((b - a) ** 2)
            dy_db = (a - value) / ((b - a) ** 2)
            dy_dc = 0.0
        elif b < value < c:
            dy_da = 0.0
            dy_db = (c - value) / ((c - b) ** 2)
            dy_dc = (value - b) / ((c - b) ** 2)
        else:
            dy_da = dy_db = dy_dc = 0.0

        return [dy_da, dy_db, dy_dc]


class TrapmFuzzificationLayer(FuzzificationLayer):
    """Трапециевидный слой фазификации"""

    def _forwardImplementation(self, universe: ndarray, parameters: ndarray):
        assert len(parameters) == 4, ERRORS["count_arg"]
        return fuzz.trapmf(universe, parameters,)

    def _backwardImplementation(self, value, parameters: ndarray):
        pass


"""
skfuzzy.membership.dsigmf(x, b1, c1, b2, c2)	Difference of two fuzzy sigmoid
membership functions.

skfuzzy.membership.gauss2mf(x, mean1, ...)	Gaussian fuzzy membership function
of two combined Gaussians.

skfuzzy.membership.gaussmf(x, mean, sigma)	Gaussian fuzzy membership function.

skfuzzy.membership.gbellmf(x, a, b, c)	Generalized Bell function fuzzy
membership generator.

skfuzzy.membership.piecemf(x, abc)	Piecewise linear membership function
(particularly used in FIRE filters).

skfuzzy.membership.pimf(x, a, b, c, d)	Pi-function fuzzy membership generator.

skfuzzy.membership.psigmf(x, b1, c1, b2, c2)	Product of two sigmoid membership
functions.

skfuzzy.membership.sigmf(x, b, c)	The basic sigmoid membership function
generator.

skfuzzy.membership.smf(x, a, b)	S-function fuzzy membership generator.

skfuzzy.membership.zmf(x, a, b)	Z-function fuzzy membership generator.
"""
