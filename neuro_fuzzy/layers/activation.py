# import skfuzzy as fuzz
from numpy import ndarray, asarray
from abc import ABC

from .base import Layer
from ..structures import Term


# from ..errors import ERRORS


class ActivationLayer(Layer, ABC):
    """Базовый слой активации"""

    result = None

    def __init__(self, term: Term):
        self.term = term
        self.coef_trust = float(1)

    def activate(self, membf: ndarray, value: float):
        new_mf = []
        for el in membf:
            new_mf.append(self._forwardImplementation(el, value))
        self.result = self.coef_trust * asarray(new_mf)
        return self.result


class MinActivationLayer(ActivationLayer):
    """Min слой активации"""

    def _forwardImplementation(self, mf: float, value: float):
        if mf <= value:
            return mf
        return value
