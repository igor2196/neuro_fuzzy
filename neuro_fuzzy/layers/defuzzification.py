import skfuzzy as fuzz
from numpy import ndarray
from abc import ABC

from .base import Layer

# from ..errors import ERRORS


class DefuzzificationLayer(Layer, ABC):
    """Базовый слой дефазификации"""

    result = None

    def __init__(self, universe: ndarray):
        self.universe = universe

    def activate(self, mf: ndarray):
        self.result = self._forwardImplementation(self.universe, mf)
        return self.result


class CentroidDefuzzificationLayer(DefuzzificationLayer):
    """Centroid слой аккумулирования"""

    def _forwardImplementation(self, universe: ndarray, mf: ndarray):
        return fuzz.defuzz(universe, mf, "centroid")
