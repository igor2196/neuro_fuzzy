# import skfuzzy as fuzz
from numpy import ndarray, asarray
from abc import ABC

from .base import Layer

from ..structures import Rule, Term, BaseTerm

# from ..errors import ERRORS

# TODO: Нужен еще один пример T нормы (что бы понять правильно ли я
#       спроектировал передачу параметров слой)


class AggregationLayer(Layer, ABC):
    """Базовый слой агрегации"""

    result = None  # type: float

    def __init__(self, rule: Rule = None, term: Term = None):
        self.rule = rule
        self.term = term

    @property
    def antecedents(self) -> BaseTerm:
        assert isinstance(self.rule, Rule)
        return self.rule.antecedents

    @property
    def consequents(self) -> BaseTerm:
        assert isinstance(self.rule, Rule)
        return self.rule.consequents

    def activate(self, values: ndarray):
        self.result = self._forwardImplementation(asarray(values))
        return self.result


class MinAggregationLayer(AggregationLayer):
    """Min слой агрегации"""

    def _forwardImplementation(self, values: ndarray) -> float:
        return values.min()


class MaxAggregationLayer(AggregationLayer):
    """Max слой агрегации"""

    def _forwardImplementation(self, values: ndarray) -> float:
        return values.max()
