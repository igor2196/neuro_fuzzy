# import skfuzzy as fuzz
from numpy import ndarray, asarray
from abc import ABC

from .base import TraningLayer

# from ..errors import ERRORS


class AccumulationLayer(TraningLayer, ABC):
    """Базовый слой аккумулирования"""

    result = None

    def activate(self, values: ndarray):
        self.result = self._forwardImplementation(asarray(values))
        return self.result

    def train(self, error, speed, coef_trust):
        return self._backwardImplementation(error, speed, coef_trust)


class MaxAccumulationLayer(AccumulationLayer):
    """Max слой аккумулирования"""

    def _forwardImplementation(self, values: ndarray):
        return values.max(axis=0)

    def _backwardImplementation(self, error, speed, coef_trust):
        # TODO: производная которую я не понимаю как считать
        return float(1)
