import copy
from typing import List, Type, Set, Iterable  # noqa: F401

from .structures import Rule, LinguisticVariable  # noqa: F401

from .layers.fuzzification import FuzzificationLayer, TrimFuzzificationLayer
from .layers.aggregation import (
    AggregationLayer,
    MinAggregationLayer,
    MaxAggregationLayer,
)
from .layers.activation import ActivationLayer, MinActivationLayer
from .layers.accumulation import AccumulationLayer, MaxAccumulationLayer
from .layers.defuzzification import (
    DefuzzificationLayer,
    CentroidDefuzzificationLayer,
)

from .errors import ERRORS


class FuzzyNetwork(object):
    """
    Нейро-нечеткая сеть

    Args:
      - rules (List[Rule]): Правила
      - fuzzification_class (FuzzificationLayer): Слой фазификации
      - t_norm_aggregation_class (AggregationLayer): Слой агрегирования T-норма
      - s_norm_aggregation_class (AggregationLayer): Слой агрегирования S-норма
      - activation_class (ActivationLayer): Слой активации правил
    """

    antecedents = []  # type: List[LinguisticVariable]
    rules = []  # type: List[Rule]
    consequents = []  # type: List[LinguisticVariable]

    fuzzification_neurons = []  # type: List[FuzzificationLayer]
    t_norm_aggregation_neurons = []  # type: List[AggregationLayer]
    s_norm_aggregation_neurons = []  # type: List[AggregationLayer]
    activation_neurons = []  # type: List[ActivationLayer]
    accumulation_neuron = None  # type: AccumulationLayer
    defuzzification_neuron = None  # type: DefuzzificationLayer

    def __init__(
        self,
        rules: List[Rule],
        fuzzification_class: Type[FuzzificationLayer] = TrimFuzzificationLayer,
        t_norm_aggregation_class: Type[AggregationLayer] = MinAggregationLayer,
        s_norm_aggregation_class: Type[AggregationLayer] = MaxAggregationLayer,
        activation_class: Type[ActivationLayer] = MinActivationLayer,
        accumulation_class: Type[AccumulationLayer] = MaxAccumulationLayer,
        defuzzification_class: Type[
            DefuzzificationLayer
        ] = CentroidDefuzzificationLayer,
    ):
        self.fuzzification_class = fuzzification_class
        self.t_norm_aggregation_class = t_norm_aggregation_class
        self.s_norm_aggregation_class = s_norm_aggregation_class
        self.activation_class = activation_class
        self.accumulation_class = accumulation_class
        self.defuzzification_class = defuzzification_class

        self._copy(rules)
        self._init_layers()

    def _copy(self, rules: List[Rule]):
        """Копирование данных"""
        self.rules = copy.deepcopy(rules)
        antecedents = set()  # type: Set[LinguisticVariable]
        consequents = set()  # type: Set[LinguisticVariable]
        for rule in self.rules:
            antecedents.update(rule._antecedents)
            consequents.update(rule._consequents)
        self.antecedents = list(antecedents)
        self.consequents = list(consequents)

    def _init_layers(self):
        """Инициализация слоев"""

        # Слой фазификации
        for antecedent in self.antecedents:
            for term in antecedent:
                neuron = self.fuzzification_class(term)
                self.fuzzification_neurons.append(neuron)

        # Слой агрегирования T-норма
        for rule in self.rules:
            neuron = self.t_norm_aggregation_class(rule=rule)
            self.t_norm_aggregation_neurons.append(neuron)

        for consequent in self.consequents:
            for term in consequent:

                # Слой агрегирования S-норма
                s_neuron = self.s_norm_aggregation_class(term=term)
                self.s_norm_aggregation_neurons.append(s_neuron)

                # Слой активации правил
                a_neuron = self.activation_class(term)
                self.activation_neurons.append(a_neuron)

        # Слой аккумулирования
        self.accumulation_neuron = self.accumulation_class()

        # Слой дефазификации
        # TODO: подумать над этим
        self.defuzzification_neuron = self.defuzzification_class(
            self.consequents[0].universe
        )

    def activate(self, values: list):
        """Активация сети"""
        assert len(values) == len(self.antecedents), ERRORS["count_inputs"]

        # Слой фазификации
        for fuzz_neuron in self.fuzzification_neurons:
            for value in values:
                if value["uuid"] == fuzz_neuron.lang.uuid:
                    fuzz_neuron.activate(value["value"])

        # Слой агрегирования T-норма
        for t_norm_neuron in self.t_norm_aggregation_neurons:
            outputs = []
            for fuzz_neuron in self.fuzzification_neurons:
                if fuzz_neuron.term in t_norm_neuron.antecedents:
                    outputs.append(fuzz_neuron.result)
            t_norm_neuron.activate(outputs)

        # Слой агрегирования S-норма
        for s_norm_neuron in self.s_norm_aggregation_neurons:
            outputs = []
            for t_norm_neuron in self.t_norm_aggregation_neurons:
                if s_norm_neuron.term in t_norm_neuron.consequents:
                    outputs.append(t_norm_neuron.result)
            s_norm_neuron.activate(outputs)

        # Слой активации правил
        for act_neuron in self.activation_neurons:
            mf = self.fuzzification_class.calcMembf(act_neuron.term)
            for s_norm_neuron in self.s_norm_aggregation_neurons:
                if s_norm_neuron.term == act_neuron.term:
                    act_neuron.activate(mf, s_norm_neuron.result)
                    break

        # Слой аккумулирования
        self.accumulation_neuron.activate(
            list(map(lambda n: n.result, self.activation_neurons))
        )

        # Слой дефазификации
        self.defuzzification_neuron.activate(self.accumulation_neuron.result)

        return self.defuzzification_neuron.result

    def train_coef_trust(self, error, speed):
        """Обучение коэффициентов доверия"""
        # Слой активации правил и аккумулирования
        for act_neuron in self.activation_neurons:
            # Корректируем коэффициент доверия
            act_neuron.coef_trust = self.accumulation_neuron.train(
                error, speed, act_neuron.coef_trust
            )

    def train_fuzzification_params(self, error, values, speed):
        """Обучение параметров функции принадлежности"""
        for fuzz_neuron in self.fuzzification_neurons:
            for value in values:
                if value["uuid"] == fuzz_neuron.lang.uuid:
                    fuzz_neuron.train(value["value"], error, speed)

    def view(self):
        """TODO: показать графики"""
        pass
