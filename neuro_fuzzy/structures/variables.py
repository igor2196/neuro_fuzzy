import uuid

from abc import ABC, abstractmethod
from typing import Dict  # noqa: F401
from numpy import ndarray

from .term import Term


class LinguisticVariable(ABC):
    """
    Лингвистическая переменная

    Args:
      - name (str): Наименование
      - universe (1d array): Универсум
    """

    def __init__(self, name: str, universe: ndarray):
        self.uuid = str(uuid.uuid1())
        self.name = name
        self.universe = universe
        self.terms = {}  # type: Dict[str, Term]

    def __getitem__(self, key: str) -> Term:
        """
        Получить терм по ключу

        Args:
          - key (str): Наименование
        """
        return self.terms[key]

    def __setitem__(self, key: str, value):
        """
        Добавить терм с ключем

        Args:
          - key (str): Наименование
          - value (ndarray or None): значение
        """
        if value is None:
            del self.terms[key]
        if not isinstance(value, list):
            raise TypeError(f"must be ndarray, not { type(value) }")
        self.terms[key] = Term(self, key, value, self.universe)

    @abstractmethod
    def __str__(self):
        raise NotImplementedError

    def __iter__(self):
        return iter(self.terms.values())

    def connect(self, value: float):
        return {"uuid": self.uuid, "value": value}


class Antecedent(LinguisticVariable):
    def __str__(self):
        return f"(Антецедент: { self.name })"


class Consequent(LinguisticVariable):
    def __str__(self):
        return f"(Консеквент: { self.name })"
