from enum import Enum


class Condition(Enum):
    OR = "OR"
    AND = "AND"
    NOT = "NOT"
