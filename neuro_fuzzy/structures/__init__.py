# flake8: noqa
from .term import Term, BaseTerm
from .variables import Antecedent, Consequent, LinguisticVariable
from .rule import Rule
from .datasets import FuzzyDataset
