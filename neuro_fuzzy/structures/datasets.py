import os

from pandas.core.frame import DataFrame

from typing import List, Type, Set, Iterable  # noqa: F401
from .variables import Antecedent
from ..errors import ERRORS


class Sample(object):
    def __init__(self, output):
        self.output = output
        self.inputs = []

    def appendInput(self, antecedent, x):
        self.inputs.append(antecedent.connect(x))

    def __str__(self):
        return f"{str(self.inputs)} : {self.output}"


class FuzzyDataset(object):
    def __init__(self, antecedents: List[Antecedent]):
        self.samples = []  # type: List[Sample]
        self.antecedents = antecedents  # TODO: deep copy

    def addSample(self, inputs: List[float], output: float):
        assert len(inputs) == len(self.antecedents), ERRORS["count_inputs"]
        sample = Sample(output)
        for el in zip(self.antecedents, inputs):
            sample.appendInput(el[0], el[1])
        self.samples.append(sample)

    def addSamplesFromPandas(self, df: DataFrame):
        for item in df.iterrows():
            inputs = item[1].values[:-1]
            output = item[1].values[-1]
            self.addSample(inputs, output)

    def __str__(self):
        s = f", {os.linesep}".join(str(item) for item in self.samples)
        return f"[{os.linesep}{s}{os.linesep}]"
