from abc import ABC, abstractproperty
from typing import List, Union
from numpy import ndarray

from .utils import Condition

if False:
    from .variables import LinguisticVariable


class BaseTerm(ABC):
    """Базовый абстрактный Терм"""

    def __or__(self, other):
        raise NotImplementedError
        # return TermAggregate(self, other, Condition.OR)

    def __and__(self, other):
        return TermAggregate(self, other, Condition.AND)

    def __invert__(self):
        pass

    def __contains__(self, term):
        return term in self.condition

    @abstractproperty
    def condition(self) -> List[Union["BaseTerm", Condition]]:
        raise NotImplementedError


class Term(BaseTerm):
    """
    Терм

    Args:
      - lang (str): Лингвистическая переменная
      - name (str): Наименование
      - parameters (1d array): параметры
    """

    def __init__(
        self,
        lang: "LinguisticVariable",  # noqa: F821
        name: str,
        parameters: ndarray,
        universe: ndarray,
    ):
        self.lang = lang
        self.name = name
        self.parameters = parameters.copy()
        self.universe = universe.copy()

    def __str__(self):
        return f"{{{ str(self.lang) }:{ self.name }}}"

    @property
    def condition(self) -> List[Union["BaseTerm", Condition]]:
        return [
            self,
        ]


class TermAggregate(BaseTerm):
    def __init__(self, term1: BaseTerm, term2: BaseTerm, kind: Condition):
        self.term1 = term1
        self.term2 = term2
        self.kind = kind

    def __str__(self):
        return "%s %s %s" % (str(self.term1), self.kind.value, str(self.term2))

    @property
    def condition(self) -> List[Union["BaseTerm", Condition]]:
        return [
            *self.term1.condition,
            self.kind,
            *self.term2.condition,
        ]
