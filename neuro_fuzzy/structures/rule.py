from typing import List

from .variables import LinguisticVariable
from .term import Term, BaseTerm


def check_term(el) -> bool:
    return isinstance(el, Term)


def get_unique_variables(condition) -> List[LinguisticVariable]:
    terms = list(filter(check_term, condition))
    return list(set(map(lambda t: t.lang, terms)))


class Rule(object):
    def __init__(self, antecedents: BaseTerm, consequents: BaseTerm):
        self.antecedents = antecedents
        self.consequents = consequents

    @property
    def _antecedents(self) -> List[LinguisticVariable]:
        return get_unique_variables(self.antecedents.condition)

    @property
    def _consequents(self) -> List[LinguisticVariable]:
        return get_unique_variables(self.consequents.condition)

    def __str__(self):
        return f"{ str(self.antecedents) } -> { str(self.consequents) }"
