from math import pow

from .structures.datasets import FuzzyDataset
from .network import FuzzyNetwork
from .errors import ERRORS


def squaredError(y_original: float, y: float):
    """Вычисление квадратичной ошибки"""
    return 1 / 2 * pow(y_original - y, 2)


class FuzzyTrainer(object):
    """
    Тренер для нейро-нечеткой сети
    """

    def __init__(
        self,
        net: FuzzyNetwork,
        ds: FuzzyDataset,
        epsilon: float = 2,
        learn_speed: float = 0.01,
        max_epochs: int = None,
    ):
        assert 0 <= learn_speed <= 1, ERRORS["speed_range"]
        self.network = net
        self.dataset = ds
        self.learn_speed = learn_speed
        self.epoch = 0
        self.max_epochs = max_epochs
        self.epsilon = epsilon

    def train_coef_trust(self):
        """Обучение коэффициентов доверия"""
        for item in self.dataset.samples:
            result = self.network.activate(item.inputs)
            err = squaredError(item.output, result)
            self.network.train_coef_trust(err, self.learn_speed)

        return True

    def train_fuzzification_params(self):
        """Обучение параметров функции принадлежности"""
        sum_err = 0
        for item in self.dataset.samples:
            result = self.network.activate(item.inputs)
            err = squaredError(item.output, result)
            self.network.train_fuzzification_params(
                err, item.inputs, self.learn_speed
            )
            sum_err = sum_err + err

        sum_err = 1 / len(self.dataset.samples) * sum_err

        return sum_err < self.epsilon

    def train(self):
        trained = True
        while trained:
            self.epoch = self.epoch + 1

            # if self.train_coef_trust():
            #     break

            if self.train_fuzzification_params():
                break

            if self.max_epochs:
                if self.epoch >= self.max_epochs:
                    trained = False
